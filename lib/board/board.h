#pragma once
#include <stdbool.h>
#include <stdint.h>

typedef enum {
    BOARD_PIN_BUTTON_1,
    BOARD_PIN_BUTTON_2,
    BOARD_PIN_BUTTON_3,
    BOARD_PIN_BUTTON_4,
    BOARD_PIN_COUNT
} BoardPin;

typedef enum DataStorageStatus {
    DataStorageStatusOk,
    DataStorageStatusBusy,
    DataStorageStatusFault,
} DataStorageStatus;

void boardInit(void);
void boardSetPin(BoardPin pin, bool state);
bool boardGetPin(BoardPin pin);
void boardGetChipUid(uint8_t * data, uint32_t size);
uint32_t boardGetCurrentTick(void);
DataStorageStatus boardDataStorageWrite(uint8_t * data, uint32_t byteCount);
DataStorageStatus boardDataStorageRead(uint32_t offset, uint8_t * data, uint32_t byteCount, uint32_t * byteReadCount);
uint32_t boardGetRtcCounter(void);
void boardSetRtcCounter(uint32_t value);
bool boardWasRtcReset(void);