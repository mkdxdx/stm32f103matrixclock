#include "stm32f10x_conf.h"
#include "board.h"
#include <stddef.h>
#include <string.h>

#define Pin_Button_1 GPIO_Pin_12
#define Pin_Button_2 GPIO_Pin_13
#define Pin_Button_3 GPIO_Pin_14
#define Pin_Button_4 GPIO_Pin_15
#define Port_Button GPIOB
#define Port_Button_Source_Cmd RCC_APB2PeriphClockCmd
#define Port_Button_Source RCC_APB2Periph_GPIOB

#define FLASH_BASE_ADDRESS   0x08000000
#define FLASH_PAGE_SIZE      1024
#define FLASH_CFG_PAGE       63
#define FLASH_PAGE_COUNT     64
#define FLASH_SIZE           (FLASH_PAGE_COUNT * FLASH_PAGE_SIZE)
#define FLASH_CFG_PAGE_COUNT 1
#define FLASH_CFG_SIZE       (FLASH_PAGE_SIZE * FLASH_CFG_PAGE_COUNT)
#define FLASH_CFG_OFFSET     (FLASH_CFG_PAGE * FLASH_PAGE_SIZE)
#define FLASH_CFG_ADDRESS    (FLASH_CFG_OFFSET + FLASH_BASE_ADDRESS)
#define FLASH_CFG_PAGE_COUNT 1

typedef struct {
    GPIO_TypeDef *port;
    uint16_t pin;
    GPIOMode_TypeDef mode;
    GPIOSpeed_TypeDef speed;
} GpioGroup;

typedef struct {
    uint16_t hw0;
    uint16_t hw1;
    uint32_t w0;
    uint32_t w1;
} UidReg;

static const GpioGroup BoardGpio[BOARD_PIN_COUNT] = {
    [BOARD_PIN_BUTTON_1] = { 
        .port = Port_Button, 
        .pin = Pin_Button_1,
        .mode = GPIO_Mode_IPD,
        .speed = GPIO_Speed_2MHz,
    },
    [BOARD_PIN_BUTTON_2] = { 
        .port = Port_Button, 
        .pin = Pin_Button_2,
        .mode = GPIO_Mode_IPD,
        .speed = GPIO_Speed_2MHz,
    },
    [BOARD_PIN_BUTTON_3] = {
        .port = Port_Button, 
        .pin = Pin_Button_3,
        .mode = GPIO_Mode_IPD,
        .speed = GPIO_Speed_2MHz,
    },
    [BOARD_PIN_BUTTON_4] = { 
        .port = Port_Button, 
        .pin = Pin_Button_4,
        .mode = GPIO_Mode_IPD,
        .speed = GPIO_Speed_2MHz,
    },
};

static volatile const UidReg *Uid = (volatile const UidReg *)(0x1FFFF7E8);
static volatile uint32_t ticks = 0;
// this goes true if rtc was not running prior to initialization
static bool wasRtcReset = false;

void SysTick_Handler(void)
{
    ticks++;
}

static void gpioInit(void)
{
    Port_Button_Source_Cmd(Port_Button_Source, ENABLE);
    for (uint32_t i = 0; i < BOARD_PIN_COUNT; i++) {
        GPIO_InitTypeDef gpio;
        gpio.GPIO_Mode = BoardGpio[i].mode;
        gpio.GPIO_Pin = BoardGpio[i].pin;
        gpio.GPIO_Speed = BoardGpio[i].speed;
        GPIO_Init(BoardGpio[i].port, &gpio);
    }
}

static void rtcInit(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	PWR_BackupAccessCmd(ENABLE);
	if ((RCC->BDCR & RCC_BDCR_RTCEN) != RCC_BDCR_RTCEN)
	{
		RCC_BackupResetCmd(ENABLE);
		RCC_BackupResetCmd(DISABLE);
		RCC_LSEConfig(RCC_LSE_ON);
		while ((RCC->BDCR & RCC_BDCR_LSERDY) != RCC_BDCR_LSERDY) {}
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
		RTC_SetPrescaler(0x7FFF);
		RCC_RTCCLKCmd(ENABLE);
		RTC_WaitForSynchro();
		wasRtcReset = true;
	}
}

void boardInit() {
    gpioInit();
    rtcInit();
    SysTick_Config(SystemCoreClock / 1000);
}

void boardSetPin(BoardPin pin, bool state)
{
    if (state)
        GPIO_SetBits(BoardGpio[pin].port, BoardGpio[pin].pin);
    else
        GPIO_ResetBits(BoardGpio[pin].port, BoardGpio[pin].pin);
}

bool boardGetPin(BoardPin pin)
{
    return GPIO_ReadInputDataBit(BoardGpio[pin].port, BoardGpio[pin].pin) != 0;
}

void boardGetChipUid(uint8_t * data, uint32_t size)
{
    uint8_t arr[12];
    for (uint32_t i = 0; i < sizeof(arr); i++) {
        arr[i] = ((uint8_t *)Uid)[i];
    }

    for (uint32_t i = 0; i < size; i++) {
        data[i] = arr[i];
    }
}

DataStorageStatus boardDataStorageWrite(uint8_t * data, uint32_t byteCount)
{
    if (byteCount == 0 || byteCount > FLASH_CFG_SIZE)
        return DataStorageStatusFault;

    FLASH_Unlock();
    FLASH_ErasePage(FLASH_CFG_ADDRESS);

    bool result;

    for (uint32_t i = 0; i < byteCount / sizeof(uint32_t) + (byteCount % sizeof(uint32_t) > 0 ? 1 : 0); i++) {
        uint32_t temp = 0;
        memcpy(&temp, &data[i * sizeof(uint32_t)], (i + 1) * sizeof(uint32_t) > byteCount
                                                    ? byteCount - i * sizeof(uint32_t)
                                                    : sizeof(uint32_t));
        result = FLASH_ProgramWord(FLASH_CFG_ADDRESS + i * sizeof(uint32_t), temp) == FLASH_COMPLETE;
        if (!result)
            break;
    }

    FLASH_Lock();

    return result;
}

DataStorageStatus boardDataStorageRead(uint32_t offset, uint8_t * data, uint32_t byteCount, uint32_t * byteReadCount)
{
    if (byteCount == 0)
        return DataStorageStatusOk;

    memcpy(data, (volatile uint8_t *)(FLASH_CFG_ADDRESS + offset), byteCount);

    return DataStorageStatusOk;
}

uint32_t boardGetCurrentTick(void)
{
    return ticks;
}

uint32_t boardGetRtcCounter(void)
{
    return RTC_GetCounter();
}

void boardSetRtcCounter(uint32_t value)
{
    RTC_SetCounter(value);
}

bool boardWasRtcReset(void)
{
    return wasRtcReset;
}