#pragma once

#include <stdint.h>

typedef struct {
    uint32_t x;
    uint32_t y;
} GfxPoint;

typedef enum {
    GFX_COLOR_BLACK,
    GFX_COLOR_RED,
    GFX_COLOR_GREEN,
    GFX_COLOR_YELLOW,
    GFX_COLOR_COUNT
} GfxColor;

void gfxInit(void);
void gfxSetPixel(const GfxPoint *p, GfxColor color);
void gfxPuts(char *str, const GfxPoint *p, GfxColor color);
void gfxGetStringSize(char *str, GfxPoint *size);
void gfxFill(GfxColor color);
void gfxGetScreenSize(GfxPoint *size);
void gfxFlush(void);