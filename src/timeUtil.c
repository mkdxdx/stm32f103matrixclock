#include "timeUtil.h"
#include <stdint.h>
#include <stddef.h>

// (UnixTime = 00:00:00 01.01.1970 = JD0 = 2440588)
#define JULIAN_DATE_BASE	2440588

void timeUtilCounterToDate(uint32_t counter, TimeUtilDate *date)
{
    // from https://blog.avislab.com/stm32-rtc_ru/
    if (date == NULL) {
        return;
    }

    unsigned long time;
	unsigned long t1, a, b, c, d, e, m;
	int year = 0;
	int mon = 0;
	int wday = 0;
	int mday = 0;
	int hour = 0;
	int min = 0;
	int sec = 0;
	uint64_t jd = 0;;
	uint64_t jdn = 0;

	jd = ((counter+43200)/(86400>>1)) + (2440587<<1) + 1;
	jdn = jd>>1;

	time = counter;
	t1 = time/60;
	sec = time - t1*60;

	time = t1;
	t1 = time/60;
	min = time - t1*60;

	time = t1;
	t1 = time/24;
	hour = time - t1*24;

	wday = jdn%7;

	a = jdn + 32044;
	b = (4*a+3)/146097;
	c = a - (146097*b)/4;
	d = (4*c+3)/1461;
	e = c - (1461*d)/4;
	m = (5*e+2)/153;
	mday = e - (153*m+2)/5 + 1;
	mon = m + 3 - 12*(m/10);
	year = 100*b + d - 4800 + (m/10);

	date->year = year;
	date->month = mon;
	date->monthDay = mday;
	date->hour = hour;
	date->minute = min;
	date->second = sec;
	date->weekDay = wday;
}

uint32_t timeUtilDateToCounter(TimeUtilDate *date) 
{
    //from https://blog.avislab.com/stm32-rtc_ru/

    if (date == NULL) {
        return 0;
    }

	uint8_t a;
	uint16_t y;
	uint8_t m;
	uint32_t JDN;

	a=(14-date->month)/12;
	y=date->year+4800-a;
	m=date->month+(12*a)-3;

	JDN=date->monthDay;
	JDN+=(153*m+2)/5;
	JDN+=365*y;
	JDN+=y/4;
	JDN+=-y/100;
	JDN+=y/400;
	JDN = JDN -32045;
	JDN = JDN - JULIAN_DATE_BASE;
	JDN*=86400;
	JDN+=(date->hour*3600);
	JDN+=(date->minute*60);
	JDN+=(date->second);

	return JDN;
}

void timeUtilCounterToTime(uint32_t counter, TimeUtilTime *time)
{
    if (time == NULL) {
        return;
    }

    time->hour = counter / 3600;
    time->minute = (counter - (3600 * time->hour)) / 60;
    time->second = (counter - (3600 * time->hour) - (time->minute * 60));
}

uint32_t timeUtilTimeToCounter(TimeUtilTime *time)
{
    if (time == NULL) {
        return 0;
    }

    return time->hour * 3600
            + time->minute * 60
            + time->second;
}