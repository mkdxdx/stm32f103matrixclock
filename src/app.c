#include "board.h"
#include "app.h"
#include "gfx.h"
#include "effect.h"
#include "clockDisplay.h"
#include <string.h>

void appInit() 
{
    boardInit();
    gfxInit();
    clockDisplayInit();
}

void appProcess() 
{
    clockDisplayProcess();
}