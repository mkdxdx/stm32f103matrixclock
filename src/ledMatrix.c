#include "ledMatrix.h"
#include "stm32f10x_conf.h"
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

#define Pin_Sr_Source_Clk (GPIO_Pin_6)
#define Pin_Sr_Source_Data (GPIO_Pin_5)
#define Pin_Sr_Source_Latch (GPIO_Pin_4)
#define Pin_Sr_Source_Reset (GPIO_Pin_3)
#define Pin_Sr_Source_Enable (GPIO_Pin_2)
#define Pin_Sr_Sink_Clk (GPIO_Pin_1)
#define Pin_Sr_Sink_Data (GPIO_Pin_0)
#define Port_Sr (GPIOA)
#define Port_Sr_Rcc (RCC_APB2Periph_GPIOA)
#define Port_Sr_Rcc_ClockCmd (RCC_APB2PeriphClockCmd)

#define Trig_Timer (TIM2)
#define Trig_Timer_Rcc (RCC_APB1Periph_TIM2)
#define Trig_Timer_Rcc_ClockCmd (RCC_APB1PeriphClockCmd)

#define DMA_Rcc (RCC_AHBPeriph_DMA1)
#define DMA_Rcc_ClockCmd (RCC_AHBPeriphClockCmd)
#define DMA_Channel (DMA1_Channel2)
#define DMA_Channel_IRQn (DMA1_Channel2_IRQn)
#define DMA_Channel_IRQHandler (DMA1_Channel2_IRQHandler)
#define DMA_Pending_IT_Bit_TC (DMA1_IT_TC2)
#define DMA_Pending_IT_Bit_HT (DMA1_IT_HT2)

// two bytes for column register (1 for red, 1 for green columns)
// four bytes for row register
#define SOURCE_REG_COUNT (2U)
#define SINK_REG_COUNT (4U)
#define BITS_PER_REG (8U)
// one bit for data setup
// one bit for clock hold
#define CLOCK_LOW_TICKS (1U)
#define CLOCK_HIGH_TICKS (1U)
// this defines how many clock ticks data stage should have
// it should contain atleast low stage and one rising stage
#define BYTES_PER_BIT ((CLOCK_LOW_TICKS) + (CLOCK_HIGH_TICKS))
#define LATCH_TICKS (2U)
#define RESET_TICKS (2U)
// this will define matrix duty cycle
// usually can be equal to row size since it's longest operation on bus
// e.g. SINK_REG_COUNT * BITS_PER_REG or greater
// this will also decrease scan frequency
#define WAIT_TICKS ((SINK_REG_COUNT) * (BITS_PER_REG) * 3U)

// offset of buffer bytes that will be shifted into row (sink) register
#define ROW_BYTE_IDX (0U)
// how many bits this register contains
#define ROW_BIT_COUNT ((SINK_REG_COUNT) * (BITS_PER_REG))
// how many bytes this register has in buffer to store data
// this is usually doubled bit count since data is set first and 
// then it must be clocked on clock front, and then it can be changed after
#define ROW_BYTE_BUFFER_LENGTH ((ROW_BIT_COUNT) * (BYTES_PER_BIT))

// offset of buffer bytes that will be shifted into column (source) register
#define COLUMN_BYTE_IDX ((ROW_BYTE_IDX) + ((ROW_BYTE_BUFFER_LENGTH) / 2))
#define COLUMN_BIT_COUNT ((SOURCE_REG_COUNT) * (BITS_PER_REG))
#define COLUMN_BYTE_BUFFER_LENGTH ((COLUMN_BIT_COUNT) * (BYTES_PER_BIT))

#define ROW_AND_COLUMN_BYTE_BUFFER_LENGTH ((ROW_BYTE_BUFFER_LENGTH) \
                                           + ((COLUMN_BYTE_BUFFER_LENGTH) - (ROW_BYTE_BUFFER_LENGTH / 2)))

// offset of buffer bytes that will contain latch front to latch stored data
#define COLUMN_LATCH_BYTE_IDX ((COLUMN_BYTE_IDX) + (COLUMN_BYTE_BUFFER_LENGTH))
#define COLUMN_LATCH_BYTE_BUFFER_LENGTH (LATCH_TICKS)

// offset of buffer bytes that will contain reset fall to clear register
#define COLUMN_RESET_BYTE_IDX ((COLUMN_LATCH_BYTE_IDX) + (COLUMN_LATCH_BYTE_BUFFER_LENGTH))
#define COLUMN_RESET_BYTE_BUFFER_LENGTH (RESET_TICKS)

// offset of bytes that will contain no changes to increase brightness
#define WAIT_TICKS_BYTE_IDX ((COLUMN_BYTE_IDX) + (COLUMN_BYTE_BUFFER_LENGTH))
#define WAIT_TICKS_BYTE_BUFFER_LENGTH ((WAIT_TICKS) * (BYTES_PER_BIT))

// offset of buffer bytes that will contain latch front to latch previously
// cleared register
#define COLUMN_RESET_LATCH_BYTE_IDX ((WAIT_TICKS_BYTE_IDX) + (WAIT_TICKS_BYTE_BUFFER_LENGTH))
#define COLUMN_RESET_LATCH_BYTE_BUFFER_LENGTH ((RESET_TICKS))

#define BUFFER_SIZE ( (ROW_AND_COLUMN_BYTE_BUFFER_LENGTH) \
                      + (COLUMN_LATCH_BYTE_BUFFER_LENGTH) \
                      + (COLUMN_RESET_BYTE_BUFFER_LENGTH) \
                      + (WAIT_TICKS_BYTE_BUFFER_LENGTH))
#define BUFFER_COUNT (2)

static DMA_InitTypeDef dma;

static uint8_t bufferData[BUFFER_COUNT * BUFFER_SIZE];
static uint8_t *bufferPtr[BUFFER_COUNT] = {
    &bufferData[0],
    &bufferData[BUFFER_SIZE]
};

static struct {
    uint8_t segment[MATRIX_COLUMNS];
} display[LED_MATRIX_SEGMENT_COLOR_COUNT] = {0};

static inline void checkAssert(int val)
{
    if (val == 0) {
        asm volatile ("BKPT #255");
    }
}

static inline void setBitValue(uint8_t *buffer, uint32_t byteIdx, uint8_t mask, bool value)
{
    // checkAssert(byteIdx < BUFFER_SIZE);

    if (value) {
        buffer[byteIdx] |= mask;
    } else {
        buffer[byteIdx] &= ~mask;
    }
}

static inline void setSourceClock(uint8_t *buffer, uint32_t bufferByteIdx, bool value)
{
    setBitValue(buffer, bufferByteIdx, Pin_Sr_Source_Clk, value);
}

static inline void setSinkClock(uint8_t *buffer, uint32_t bufferByteIdx, bool value)
{
    setBitValue(buffer, bufferByteIdx, Pin_Sr_Sink_Clk, value);
}

static inline void setSourceData(uint8_t *buffer, uint32_t bufferByteIdx, bool value)
{
    setBitValue(buffer, bufferByteIdx, Pin_Sr_Source_Data, value);
}

static inline void setSinkData(uint8_t *buffer, uint32_t bufferByteIdx, bool value)
{
    setBitValue(buffer, bufferByteIdx, Pin_Sr_Sink_Data, value);
}

static inline void setSourceLatch(uint8_t *buffer, uint32_t bufferByteIdx, bool value)
{
    setBitValue(buffer, bufferByteIdx, Pin_Sr_Source_Latch, value);
}

static inline void setSourceReset(uint8_t *buffer, uint32_t bufferByteIdx, bool value)
{
    setBitValue(buffer, bufferByteIdx, Pin_Sr_Source_Reset, value);
}

static inline void setSourceEnable(uint8_t *buffer, uint32_t bufferByteIdx, bool value)
{
   setBitValue(buffer, bufferByteIdx, Pin_Sr_Source_Enable, value);
}

static inline void writeSinkBit(uint8_t *buffer, uint32_t bitIdx, bool set)
{
    setSinkData(buffer, ROW_BYTE_IDX + bitIdx * 2, set);
    setSinkData(buffer, ROW_BYTE_IDX + bitIdx * 2 + 1, set);
}

static inline void writeColumnBit(uint8_t *buffer, uint32_t bitIdx, bool set)
{
    setSourceData(buffer, COLUMN_BYTE_IDX + bitIdx * 2, set);
    setSourceData(buffer, COLUMN_BYTE_IDX + bitIdx * 2 + 1, set);
}

void DMA_Channel_IRQHandler(void)
{
    static volatile uint32_t currentColumnIndex = 0;
    static volatile uint32_t currentRowIndex = 0;
    static volatile LedMatrixSegmentColor color = 0;
    
    uint8_t *updateBuffer = NULL;
    uint8_t *transferBuffer = NULL;

    if (DMA_GetFlagStatus(DMA_Pending_IT_Bit_TC) == SET) {
        // full transfer complete, second buffer free
        DMA_ClearITPendingBit(DMA_Pending_IT_Bit_TC);
        updateBuffer = bufferPtr[1];
        transferBuffer = bufferPtr[0];
    } else if (DMA_GetFlagStatus(DMA_Pending_IT_Bit_HT) == SET) {
        // half transfer complete, first buffer free
        DMA_ClearITPendingBit(DMA_Pending_IT_Bit_HT);
        updateBuffer = bufferPtr[0];
        transferBuffer = bufferPtr[1];
    }

    // update buffers
    if (updateBuffer != NULL && transferBuffer != NULL) {
        memcpy(updateBuffer, transferBuffer, ROW_AND_COLUMN_BYTE_BUFFER_LENGTH);

        // clear previous row in sink
        writeSinkBit(updateBuffer, currentRowIndex, false);
        // clear column
        writeColumnBit(updateBuffer, currentColumnIndex + color * BITS_PER_REG, false);

        currentRowIndex++;
        // skip rows that are not connected
        // each 8th pin of row register is empty
        if (currentRowIndex % BITS_PER_REG >= MATRIX_ROWS) {
            currentRowIndex++;
        }
        if (currentRowIndex >= (BITS_PER_REG * SINK_REG_COUNT)) {
            currentRowIndex = 0;
            currentColumnIndex++;
            if (currentColumnIndex >= (BITS_PER_REG)) {
                currentColumnIndex = 0;    
                color++;
                if (color >= LED_MATRIX_SEGMENT_COLOR_COUNT) {
                    color = 0;
                }            
            }
        }

        // set next row - use value from display
        uint8_t *segPtr = display[color].segment;
        const uint32_t displaySegmentIndex = currentRowIndex / BITS_PER_REG * BITS_PER_REG + currentColumnIndex;
        const uint32_t displaySegmentBitIndex = currentRowIndex % BITS_PER_REG;
        const uint8_t displaySegmentMask = 1 << displaySegmentBitIndex;
        const bool rowValue = (segPtr[displaySegmentIndex] & displaySegmentMask) != 0;
        writeSinkBit(updateBuffer, currentRowIndex, rowValue);
        // set next column - can use value from display
        writeColumnBit(updateBuffer, currentColumnIndex + color * BITS_PER_REG, true);  
    }
}

static void initBuffers(void)
{
    for (uint32_t bufIdx = 0; bufIdx < BUFFER_COUNT; bufIdx++) {
        const uint8_t *buffer = bufferPtr[bufIdx];
        uint32_t bufferByteIdx;  

        // disable reset
        bufferByteIdx = 0;
        for (uint32_t i = 0; i < BUFFER_SIZE; i++) {
            setSourceReset(buffer, bufferByteIdx++, true);
        }  

        // set clock fronts for sink stage
        bufferByteIdx = ROW_BYTE_IDX;
        for (uint32_t i = 0; i < SINK_REG_COUNT; i++) {
            for (uint32_t bit = 0; bit < BITS_PER_REG; bit++) {
                setSinkClock(buffer, bufferByteIdx++, false);
                setSinkClock(buffer, bufferByteIdx++, true);
            }
        }

        // set clock fronts for source column stage + latch
        bufferByteIdx = COLUMN_BYTE_IDX;
        for (uint32_t i = 0; i < SOURCE_REG_COUNT; i++) {
            for (uint32_t bit = 0; bit < BITS_PER_REG; bit++) {
                setSourceClock(buffer, bufferByteIdx++, false);
                setSourceClock(buffer, bufferByteIdx++, true);
            }
        }

        bufferByteIdx = COLUMN_LATCH_BYTE_IDX;
        setSourceLatch(buffer, bufferByteIdx++, true);
        setSourceLatch(buffer, bufferByteIdx++, false);

        bufferByteIdx = COLUMN_RESET_BYTE_IDX;
        setSourceReset(buffer, bufferByteIdx++, false);
        setSourceReset(buffer, bufferByteIdx++, true);

        bufferByteIdx = COLUMN_RESET_LATCH_BYTE_IDX;
        setSourceLatch(buffer, bufferByteIdx++, true);
        setSourceLatch(buffer, bufferByteIdx++, false);
    }
}

void ledMatrixInit(void)
{
    // initialize pins
    GPIO_InitTypeDef gpio;

    Port_Sr_Rcc_ClockCmd(Port_Sr_Rcc, ENABLE);
    GPIO_StructInit(&gpio);
    gpio.GPIO_Mode = GPIO_Mode_Out_PP;
    gpio.GPIO_Pin  = Pin_Sr_Source_Clk | Pin_Sr_Source_Latch 
                     | Pin_Sr_Source_Data | Pin_Sr_Sink_Clk
                     | Pin_Sr_Sink_Data | Pin_Sr_Source_Reset
                     | Pin_Sr_Source_Enable;
    gpio.GPIO_Speed= GPIO_Speed_50MHz;
    GPIO_Init(Port_Sr, &gpio);
    Port_Sr->ODR = 0;

    // initialize buffers with base clock/latch bits
    initBuffers();

    // initialize dma to write stuff to gpio ODR register
    DMA_Rcc_ClockCmd(DMA_Rcc, ENABLE);
    DMA_DeInit(DMA_Channel);
    DMA_StructInit(&dma);
    dma.DMA_Mode = DMA_Mode_Circular;
    dma.DMA_DIR = DMA_DIR_PeripheralDST;
    dma.DMA_M2M = DMA_M2M_Disable;
    dma.DMA_BufferSize = sizeof(bufferData);
    dma.DMA_MemoryBaseAddr = (uint32_t)(bufferPtr[0]); // source buffer data address
    dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
    dma.DMA_PeripheralBaseAddr = (uint32_t)(&(Port_Sr->ODR));
    dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    dma.DMA_Priority = DMA_Priority_Medium;
    DMA_Init(DMA_Channel, &dma);
    DMA_ClearITPendingBit(DMA_Pending_IT_Bit_TC);
    DMA_ClearITPendingBit(DMA_Pending_IT_Bit_HT);
    DMA_ITConfig(DMA_Channel, DMA_IT_TC, ENABLE);
    DMA_ITConfig(DMA_Channel, DMA_IT_HT, ENABLE);
    NVIC_EnableIRQ(DMA_Channel_IRQn);
    DMA_Cmd(DMA_Channel, ENABLE);

    // initialize timer to trigger DMA transfers
    TIM_TimeBaseInitTypeDef xferTimer;

    xferTimer.TIM_ClockDivision = TIM_CKD_DIV1;
    xferTimer.TIM_CounterMode = TIM_CounterMode_Up;
    xferTimer.TIM_Prescaler = 0;
    // period should yield around 2-3MHz on shift register clock lines
    // on optimization (Os, O2) this value can be set down to 14 @ 72MHz
    xferTimer.TIM_Period = 1;
    xferTimer.TIM_RepetitionCounter = 0;
    Trig_Timer_Rcc_ClockCmd(Trig_Timer_Rcc, ENABLE);
    TIM_DeInit(Trig_Timer);
    TIM_TimeBaseInit(Trig_Timer, &xferTimer);
    TIM_DMACmd(Trig_Timer, TIM_DMA_Update, ENABLE);
    
    // start timer
    TIM_Cmd(Trig_Timer, ENABLE);
}

void ledMatrixSetPixel(LedMatrixSegmentColor color, uint32_t x, uint32_t y, 
                       bool set)
{
    if (color >= LED_MATRIX_SEGMENT_COLOR_COUNT || x >= MATRIX_COLUMNS
        || y >= MATRIX_ROWS) {
        return;
    }

    if (set) {
        display[color].segment[x] |= 1 << y;
    } else {
        display[color].segment[x] &= ~(1 << y);
    }
}

void ledMatrixSetColumn(LedMatrixSegmentColor color, uint32_t column, uint8_t data)
{
    if (color >= LED_MATRIX_SEGMENT_COLOR_COUNT || column >= MATRIX_COLUMNS) {
        return;
    }

    display[color].segment[column] = data;
}