#include "effect.h"
#include "ledMatrix.h"
#include "board.h"
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#define FRAME_PERIOD_MS (40U)
#define RECTANGLE_COUNT (6U)

typedef enum {
    DIRECTION_UP,
    DIRECTION_DOWN,
    DIRECTION_LEFT,
    DIRECTION_RIGHT,
    DIRECTION_COUNT
} Direction;

typedef struct {
    int32_t x;
    int32_t y;
    uint32_t w;
    uint32_t h;
    Direction direction;
    LedMatrixSegmentColor color;
    uint32_t internal;
} Rectangle;

static uint32_t timestamp = 0;

static Rectangle rectangles[RECTANGLE_COUNT];

static uint8_t rollUnsigned(uint8_t val, uint8_t limit)
{
    return (val + 1) % limit;
}

static void getNewRect(Rectangle *rect)
{
    if (rect == NULL) {
        return;
    }

    enum {
        START_AT_LEFT_TOP,
        START_AT_LEFT_BOTTOM,
        START_AT_RIGHT_TOP,
        START_AT_RIGHT_BOTTOM,
        START_AT_LEFT_CENTER,
        START_AT_RIGHT_CENTER,
        START_AT_TOP_CENTER,
        START_AT_BOTTOM_CENTER,
        START_AT_COUNT
    };

    enum {
        BLOCK_SIZE_MEDIUM,
        BLOCK_SIZE_SMALL,
        BLOCK_SIZE_BIG,
        BLOCK_SIZE_COUNT
    };

    static const struct {
        int32_t x;
        int32_t y;
    } StartPosition[] = {
        [START_AT_LEFT_TOP] = {0, 0},
        [START_AT_LEFT_BOTTOM] = {0, MATRIX_ROWS - 1},
        [START_AT_RIGHT_TOP] = {MATRIX_COLUMNS - 1, 0},
        [START_AT_RIGHT_BOTTOM] = {MATRIX_COLUMNS - 1, MATRIX_ROWS - 1},
        [START_AT_LEFT_CENTER] = {0, MATRIX_ROWS / 2},
        [START_AT_RIGHT_CENTER] = {MATRIX_COLUMNS - 1, MATRIX_ROWS / 2},
        [START_AT_TOP_CENTER] = {MATRIX_COLUMNS / 2, 0},
        [START_AT_BOTTOM_CENTER] = {MATRIX_COLUMNS / 2, MATRIX_ROWS - 1},
    };

    static const struct {
        uint32_t w;
        uint32_t h;
    } BlockSize[] = {
        [BLOCK_SIZE_MEDIUM] = {4, 4},
        [BLOCK_SIZE_SMALL] = {2, 2},
        [BLOCK_SIZE_BIG] = {6, 6},
    };

    static uint8_t startAt = START_AT_LEFT_TOP;
    static uint8_t blockSize = BLOCK_SIZE_SMALL;
    static uint8_t color = LED_MATRIX_SEGMENT_COLOR_RED;

    startAt = rollUnsigned(startAt, START_AT_COUNT);
    blockSize = rollUnsigned(blockSize, BLOCK_SIZE_COUNT);
    color = rollUnsigned(color, LED_MATRIX_SEGMENT_COLOR_COUNT);

    int32_t startX = StartPosition[startAt].x;
    int32_t startY = StartPosition[startAt].y;
    uint32_t width = BlockSize[blockSize].w;
    uint32_t height = BlockSize[blockSize].h;
    Direction direction = DIRECTION_UP;;

    switch (startAt) {
    case START_AT_LEFT_BOTTOM:
        startY = startY - (height - 1);
    case START_AT_LEFT_TOP:
    case START_AT_LEFT_CENTER:
        startX = startX - (width - 1);
        direction = DIRECTION_RIGHT;
        break;
    case START_AT_RIGHT_BOTTOM:
        startY = startY - (height - 1);
    case START_AT_RIGHT_TOP:
    case START_AT_RIGHT_CENTER:
        direction = DIRECTION_LEFT;
        break;
    case START_AT_TOP_CENTER:
        startY = startY - (height - 1);
        direction = DIRECTION_DOWN;
        break;
    case START_AT_BOTTOM_CENTER:
        direction = DIRECTION_UP;
        break;
    default:
        break;
    }

    rect->h = height;
    rect->w = width;
    rect->x = startX;
    rect->y = startY;
    rect->color = color;
    rect->direction = direction;
    rect->internal = 0;
}

static bool isRectOffscreen(Rectangle *rect)
{
    if (rect == NULL) {
        return true;
    }

    switch (rect->direction) {
    case DIRECTION_UP:
        return rect->y + (int32_t)rect->h <= 0;
    case DIRECTION_DOWN:
        return rect->y >= MATRIX_ROWS;
    case DIRECTION_LEFT:
        return rect->x + (int32_t)rect->w <= 0;
    case DIRECTION_RIGHT:
        return rect->x >= MATRIX_COLUMNS;
    default:
        return true;
    }
}

static void rectAdvance(Rectangle *rect)
{
    if (rect == NULL) {
        return;
    }

    rect->internal++;
    if (rect->internal >= (rect->h + rect->w) / 2) {
        rect->internal = 0;

        switch (rect->direction) {
        case DIRECTION_UP:
            rect->y--;
            break;
        case DIRECTION_DOWN:
            rect->y++;
            break;
        case DIRECTION_LEFT:
            rect->x--;
            break;
        case DIRECTION_RIGHT:
            rect->x++;
            break;
        }
    }
}

static void drawRect(Rectangle *rect)
{
    if (rect == NULL) {
        return;
    }

    for (uint32_t h = 0; h < rect->h; h++) {
        for (uint32_t w = 0; w < rect->w; w++) {
            ledMatrixSetPixel(rect->color, 
                              rect->x + w, 
                              rect->y + h, 
                              true);
        }
    }
}

static void advance(void)
{
    for (uint32_t rect = 0; rect < RECTANGLE_COUNT; rect++) {
        rectAdvance(&rectangles[rect]);
        if (isRectOffscreen(&rectangles[rect])) {
            getNewRect(&rectangles[rect]);
        }
    }
}

static void render(void)
{
    // clear matrix
    for (uint32_t x = 0; x < MATRIX_COLUMNS; x++) {
        for (uint32_t y = 0; y < MATRIX_ROWS; y++) {
            ledMatrixSetPixel(LED_MATRIX_SEGMENT_COLOR_RED, x, y, false);
            ledMatrixSetPixel(LED_MATRIX_SEGMENT_COLOR_GREEN, x, y, false);
        }
    }

    for (uint32_t rect = 0; rect < RECTANGLE_COUNT; rect++) {
        drawRect(&rectangles[rect]);
    }
}

void effectInit(void)
{
    timestamp = boardGetCurrentTick();
    for (uint32_t i = 0; i < RECTANGLE_COUNT; i++) {
        getNewRect(&rectangles[i]);
    }
}

void effectProcess(void)
{
    if (boardGetCurrentTick() - timestamp >= FRAME_PERIOD_MS) {
        timestamp = boardGetCurrentTick();
        advance();
        render();
    }
}