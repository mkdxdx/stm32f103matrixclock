#include "clockDisplay.h"
#include "gfx.h"
#include "board.h"
#include "timeUtil.h"
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#define UTOA_BUFFER_LEN (11U)
#define FRAME_UPDATE_PERIOD_MS (20U)
#define BUTTON_HOLD_TIME_MS (1000U)
#define TICKER_POSITION_MAX (7U)
#define TICKER_UPDATE_FAST_MS (100U)
#define TICKER_UPDATE_MEDIUM_MS (500U)
#define TICKER_UPDATE_SLOW_MS (1000U)
#define BLINK_PERIOD_MS (500U)

typedef enum {
    BUTTON_1,
    BUTTON_2,
    BUTTON_3,
    BUTTON_4,
    BUTTON_COUNT
} Button;

typedef enum {
    BUTTON_EVENT_PRESS,
    BUTTON_EVENT_HOLD,
    BUTTON_EVENT_COUNT
} ButtonEvent;

static enum Mode {
    MODE_NORMAL,
    MODE_TIMER_DOWN,
    MODE_TIMER_UP,
    MODE_SET_TIME,
    MODE_COUNT,
} currentMode = MODE_NORMAL;
static enum {
    SET_TIME_HOUR,
    SET_TIME_MINUTE,
    SET_TIME_DAY,
    SET_TIME_MONTH,
    SET_TIME_YEAR,
    SET_TIME_COUNT
} setTimeMode = SET_TIME_HOUR;
static enum {
    TICKER_DIRECTION_UP,
    TICKER_DIRECTION_DOWN,
    TICKER_DIRECTION_COUNT
} tickerDirection = TICKER_DIRECTION_DOWN;
static enum {
    TICKER_SPEED_STOP,
    TICKER_SPEED_FAST,
    TICKER_SPEED_MEDIUM,
    TICKER_SPEED_SLOW,
    TICKER_SPEED_COUNT
} tickerSpeed = TICKER_SPEED_SLOW;
static uint32_t tickerYPosition = 0;
static bool isClockNotSet = false;
static enum {
    COUNT_DOWN_STOPPED,
    COUNT_DOWN_PAUSED,
    COUNT_DOWN_RUNNING,
    COUNT_DOWN_TIMED_OUT,
} countDownState = COUNT_DOWN_STOPPED;
static enum {
    COUNT_DOWN_SET_MINUTES,
    COUNT_DOWN_SET_HOURS,
    COUNT_DOWN_SET_COUNT
} countDownSet = COUNT_DOWN_SET_MINUTES;
static uint32_t countDownTime = 0;
static uint32_t countDownPausedTime = 0;
static uint32_t countDownStartTime = 0;
static uint32_t countDownBlinkTick = 0;
static bool countDownBlinkState = false;
static enum {
    COUNT_UP_STOPPED,
    COUNT_UP_PAUSED,
    COUNT_UP_RUNNING,
} countUpState = COUNT_UP_STOPPED;
static uint32_t countUpPausedTime = 0;
static uint32_t countUpStartTime = 0;

static void utoa(uint32_t num, char buff[UTOA_BUFFER_LEN])
{
    uint32_t len = 0;
    uint32_t unum = num;

    memset(buff, '\0', UTOA_BUFFER_LEN);
    // calculate string length
    do {
        unum /= 10;
        len++;
    } while (unum && len < UTOA_BUFFER_LEN);

    for (int32_t i = len - 1; i >= 0; i--) {
        buff[i] = (char) (num % 10) + '0';
        num /= 10;
    }
}

static void updateMode(void)
{
    switch (currentMode) {
    case MODE_NORMAL:
        break;
    case MODE_SET_TIME:
        break;
    case MODE_TIMER_DOWN:
        if (countDownState == COUNT_DOWN_RUNNING) {
            int64_t diff = (int64_t)countDownTime - (boardGetRtcCounter() - countDownStartTime);
            if (diff <= 0) {
                countDownBlinkTick = boardGetCurrentTick();
                tickerSpeed = TICKER_SPEED_STOP;
                countDownState = COUNT_DOWN_TIMED_OUT;
            }
        }
        if (countDownState == COUNT_DOWN_TIMED_OUT) {
            if (boardGetCurrentTick() - countDownBlinkTick >= BLINK_PERIOD_MS) {
                countDownBlinkTick = boardGetCurrentTick();
                countDownBlinkState = !countDownBlinkState;
            }
        }
        break;
    case MODE_TIMER_UP:
        break;
    default:
        break;
    }
}

static void drawTime(TimeUtilTime *time, bool hourFirst,
                     GfxColor leftColor, GfxColor rightColor)
{
    static const GfxPoint LeftPos = {0, 0};
    static const GfxPoint RightPos = {18, 0};
    char string[UTOA_BUFFER_LEN];

    if (time == NULL) {
        return;
    }

    uint32_t left = hourFirst ? time->hour : time->minute;
    uint32_t right = hourFirst ? time->minute : time->second;
    if (left > 99) {
        left = 99;
    }
    if (right > 99) {
        right = 99;
    }

    if (left < 10) {
        string[0] = '0';
        utoa(left, &string[1]);
    } else {
        utoa(left, string);
    }
    gfxPuts(string, &LeftPos, leftColor);
    if (right < 10) {
        string[0] = '0';
        utoa(right, &string[1]);
    } else {
        utoa(right, string);
    }
    gfxPuts(string, &RightPos, rightColor);
}

static void enterMode(enum Mode newMode) {
    switch (newMode) {
    case MODE_NORMAL:
        tickerSpeed = TICKER_SPEED_SLOW;
        tickerDirection = TICKER_DIRECTION_DOWN;
        break;
    case MODE_TIMER_UP:
        tickerSpeed = TICKER_SPEED_STOP;
        tickerYPosition = 0;
        tickerDirection = TICKER_DIRECTION_UP;
        countUpState = COUNT_UP_STOPPED;
        break;
    case MODE_TIMER_DOWN: {
        countDownSet = COUNT_DOWN_SET_MINUTES;
        tickerSpeed = TICKER_SPEED_STOP;
        tickerYPosition = 0;
        tickerDirection = TICKER_DIRECTION_DOWN;
        countDownState = COUNT_DOWN_STOPPED;
    } break;
    case MODE_SET_TIME:
        isClockNotSet = false;
        setTimeMode = SET_TIME_HOUR;
        tickerDirection = TICKER_DIRECTION_DOWN;
        tickerSpeed = TICKER_SPEED_SLOW;
        break;
    default:
        break;
    }
    currentMode = newMode;
}

static void drawTicker(void)
{
    static const GfxPoint TickerStartPos = {15, 0};
    static const uint32_t TickerWidth = 2;
    static const uint32_t TickerHeight = 2;
    GfxColor color = GFX_COLOR_BLACK;

    switch (currentMode) {
    case MODE_NORMAL:
    case MODE_TIMER_UP:
        color = GFX_COLOR_GREEN;
        break;
    case MODE_SET_TIME:
        color = GFX_COLOR_YELLOW;
        break;
    case MODE_TIMER_DOWN:
        color = GFX_COLOR_RED;
        break;
    default:
        break;
    }

    for (uint32_t x = 0; x < TickerWidth; x++) {
        for (uint32_t y = 0; y < TickerHeight; y++) {
            GfxPoint pos;
            pos.x = TickerStartPos.x + x;
            if (tickerYPosition >= TICKER_POSITION_MAX - 1) {
                pos.y = 0;
                gfxSetPixel(&pos, color);
            }
            pos.y = tickerYPosition + TickerStartPos.y + y;
            gfxSetPixel(&pos, color);
        }
    }
}

static void drawNormalMode(void)
{
    TimeUtilDate date;
    TimeUtilTime time;
    GfxColor color = isClockNotSet ? GFX_COLOR_YELLOW : GFX_COLOR_GREEN;

    timeUtilCounterToDate(boardGetRtcCounter(), &date);
    time.hour = date.hour;
    time.minute = date.minute;
    time.second = date.second;
    drawTime(&time, true, color, color);
}

static void drawSetTime(void)
{
    TimeUtilDate date;
    TimeUtilTime time;

    timeUtilCounterToDate(boardGetRtcCounter(), &date);
    time.hour = date.hour;
    time.minute = date.minute;
    time.second = date.second;
    switch (setTimeMode) {
    case SET_TIME_HOUR:
    case SET_TIME_MINUTE:
        drawTime(&time, true, 
                 setTimeMode == SET_TIME_HOUR ? GFX_COLOR_YELLOW : GFX_COLOR_RED,
                 setTimeMode == SET_TIME_MINUTE ? GFX_COLOR_YELLOW : GFX_COLOR_RED);
        break;
    default:
        break;
    }
    
}

static void drawTimerDown(void)
{
    TimeUtilTime time = {0, 0, 0};
    bool hourMinutes = false;
    GfxColor leftColor = GFX_COLOR_RED;
    GfxColor rightColor = GFX_COLOR_RED;
    switch (countDownState) {
    case COUNT_DOWN_STOPPED: 
    case COUNT_DOWN_TIMED_OUT:
        timeUtilCounterToTime(countDownTime, &time);
        hourMinutes = true;
        if (countDownState == COUNT_DOWN_STOPPED 
            && countDownSet == COUNT_DOWN_SET_MINUTES) {
            rightColor = GFX_COLOR_YELLOW;
        }
        if (countDownState == COUNT_DOWN_STOPPED 
            && countDownSet == COUNT_DOWN_SET_HOURS) {
            leftColor = GFX_COLOR_YELLOW;
        }
        if (countDownState == COUNT_DOWN_TIMED_OUT) {
            if (countDownBlinkState) {
                gfxFill(GFX_COLOR_YELLOW);
            }
        }
        break;
    case COUNT_DOWN_PAUSED:
        timeUtilCounterToTime(countDownTime - (countDownPausedTime - countDownStartTime),
                              &time);
        hourMinutes = time.hour >= 1;
        break;
    case COUNT_DOWN_RUNNING:
        timeUtilCounterToTime(countDownTime - (boardGetRtcCounter() - countDownStartTime), 
                              &time);
        hourMinutes = time.hour >= 1;
        break;
    }

    drawTime(&time, hourMinutes, leftColor, rightColor);
}

static void drawTimerUp(void)
{
    TimeUtilTime time = {0, 0, 0};
    switch (countUpState) {
    case COUNT_UP_STOPPED:
        break;
    case COUNT_UP_PAUSED:
        timeUtilCounterToTime(countUpPausedTime - countUpStartTime, 
                              &time);
        break;
    case COUNT_UP_RUNNING:
        timeUtilCounterToTime(boardGetRtcCounter() - countUpStartTime, 
                              &time);
        break;
    } 
    drawTime(&time, time.hour >= 1, GFX_COLOR_RED, GFX_COLOR_RED);
}

static void processButtonEventNormalMode(Button button, ButtonEvent event)
{
    switch (button) {
    default:
        (void)event;
        break;
    }
}

static void processButtonEventTimerDown(Button button, ButtonEvent event)
{
    switch (button) {
    case BUTTON_1:
        if (event == BUTTON_EVENT_PRESS) {
            switch (countDownState) {
            case COUNT_DOWN_STOPPED:
                countDownState = COUNT_DOWN_RUNNING;
                countDownStartTime = boardGetRtcCounter() - 1;
                tickerSpeed = TICKER_SPEED_FAST;
                break;
            case COUNT_DOWN_RUNNING:
                countDownState = COUNT_DOWN_PAUSED;
                countDownPausedTime = boardGetRtcCounter();
                tickerSpeed = TICKER_SPEED_STOP;
                break;
            case COUNT_DOWN_PAUSED:
                countDownState = COUNT_DOWN_RUNNING;
                countDownStartTime += boardGetRtcCounter() - countDownPausedTime;
                tickerSpeed = TICKER_SPEED_FAST;
                break;
            case COUNT_DOWN_TIMED_OUT:
                enterMode(MODE_TIMER_DOWN);
                break;
            }
        } else if (event == BUTTON_EVENT_HOLD) {
            switch (countDownState) {
            case COUNT_DOWN_STOPPED:
                countDownSet++;
                if (countDownSet >= COUNT_DOWN_SET_COUNT) {
                    countDownSet = 0;
                }
                break;
            default:
                break;
            }
        }
        break;
    case BUTTON_2:
    case BUTTON_3:
        if (event == BUTTON_EVENT_PRESS) {
            if (countDownState == COUNT_DOWN_STOPPED) {
                TimeUtilTime currentTime;
                timeUtilCounterToTime(countDownTime, &currentTime);
                currentTime.second = 1;
                switch (countDownSet) {
                case COUNT_DOWN_SET_HOURS:
                    if (button == BUTTON_2) {
                        currentTime.hour++;
                        if (currentTime.hour > 99) {
                            currentTime.hour = 0;
                        }
                    } else {
                        currentTime.hour--;
                        if (currentTime.hour > 99) {
                            currentTime.hour = 99;
                        }
                    }
                    break;
                case COUNT_DOWN_SET_MINUTES:
                    if (button == BUTTON_2) {
                        currentTime.minute++;
                        if (currentTime.minute >= 60) {
                            currentTime.minute = 1;
                        }
                    } else {
                        currentTime.minute--;
                        if (currentTime.minute < 1) {
                            currentTime.minute = 59;
                        }
                    }
                    break;
                default:
                    break;
                }
                countDownTime = timeUtilTimeToCounter(&currentTime);
            }
        }
        break;
    case BUTTON_4:
        if (event == BUTTON_EVENT_PRESS) {
            if (countDownState == COUNT_DOWN_PAUSED || countDownState == COUNT_DOWN_TIMED_OUT) {
                enterMode(MODE_TIMER_DOWN);
            }
        }
        break;
    default:
        break;
    }
}

static void processButtonEventTimerUp(Button button, ButtonEvent event)
{
    switch (button) {
    case BUTTON_1:
        if (event == BUTTON_EVENT_PRESS) {
            switch (countUpState) {
            case COUNT_UP_STOPPED:
                countUpState = COUNT_UP_RUNNING;
                countUpStartTime = boardGetRtcCounter();
                tickerSpeed = TICKER_SPEED_FAST;
                break;
            case COUNT_UP_RUNNING:
                countUpState = COUNT_UP_PAUSED;
                tickerSpeed = TICKER_SPEED_STOP;
                countUpPausedTime = boardGetRtcCounter();
                break;
            case COUNT_UP_PAUSED:
                countUpStartTime += boardGetRtcCounter() - countUpPausedTime;
                countUpState = COUNT_UP_RUNNING;
                tickerSpeed = TICKER_SPEED_FAST;
                break;
            default:
                break;
            }
        }
        break;
    case BUTTON_4:
        if (event == BUTTON_EVENT_PRESS 
            && countUpState == COUNT_UP_PAUSED) {
            countUpState = COUNT_UP_STOPPED;
            countUpStartTime = boardGetRtcCounter();
        }
        break;
    default:
        break;
    }
}

static void processButtonEventSetTime(Button button, ButtonEvent event)
{
    switch (button) {
    case BUTTON_1:
        if (event == BUTTON_EVENT_PRESS) {
            setTimeMode++;
            if (setTimeMode > SET_TIME_MINUTE) {
                setTimeMode = 0;
            }
        }
        break;
    case BUTTON_2:
    case BUTTON_3:
        if (event == BUTTON_EVENT_PRESS) {
            TimeUtilDate date;
            timeUtilCounterToDate(boardGetRtcCounter(), &date);
            switch (setTimeMode) {
            case SET_TIME_HOUR:
                if (button == BUTTON_2) {
                    date.hour++;
                    if (date.hour >= 24) {
                        date.hour = 0;
                    } 
                } else {
                    date.hour--;
                    if (date.hour >= 24) {
                        date.hour = 23;
                    } 
                }
                break;
            case SET_TIME_MINUTE:
                if (button == BUTTON_2) {
                    date.minute++;
                    if (date.minute >= 60) {
                        date.minute = 0;
                    }
                } else {
                    date.minute--;
                    if (date.minute >= 60) {
                        date.minute = 59;
                    }
                }
                break;
            default:
                break;
            }
            boardSetRtcCounter(timeUtilDateToCounter(&date));
        }
        break;
    default:
        break;
    }
}

static void updateTicker(void)
{
    static uint32_t lastTickerUpdate = 0;
    uint32_t tickerTimeout = 0;
    switch (tickerSpeed) {
    case TICKER_SPEED_FAST:
        tickerTimeout = TICKER_UPDATE_FAST_MS;
        break;
    case TICKER_SPEED_MEDIUM:
        tickerTimeout = TICKER_UPDATE_MEDIUM_MS;
        break;
    case TICKER_SPEED_SLOW:
        tickerTimeout = TICKER_UPDATE_SLOW_MS;
        break;
    default:
        break;
    }

    if (boardGetCurrentTick() - lastTickerUpdate >= tickerTimeout
        && tickerTimeout != 0) {
        lastTickerUpdate = boardGetCurrentTick();
        switch (tickerDirection) {
        case TICKER_DIRECTION_UP:
            tickerYPosition--;
            if (tickerYPosition >= TICKER_POSITION_MAX) {
                tickerYPosition = TICKER_POSITION_MAX - 1;
            }
            break;
        case TICKER_DIRECTION_DOWN:
            tickerYPosition++;
            if (tickerYPosition >= TICKER_POSITION_MAX) {
                tickerYPosition = 0;
            }
            break;
        default:
            break;
        }
    }
}

static void processButtonEvent(Button button, ButtonEvent event)
{
    if (button == BUTTON_4 && event == BUTTON_EVENT_HOLD) {
        currentMode++;
        if (currentMode >= MODE_COUNT) {
            currentMode = 0;
        }
        enterMode(currentMode);
    } else {
        switch (currentMode) {
        case MODE_NORMAL:
            processButtonEventNormalMode(button, event);
            break;
        case MODE_TIMER_DOWN:
            processButtonEventTimerDown(button, event);
            break;
        case MODE_TIMER_UP:
            processButtonEventTimerUp(button, event);
            break;
        case MODE_SET_TIME:
            processButtonEventSetTime(button, event);
            break;
        default:
            break;
        }
    }
    
}

static void updateButtons(void)
{
    static const BoardPin ButtonPin[BUTTON_COUNT] = {
        [BUTTON_1] = BOARD_PIN_BUTTON_1,
        [BUTTON_2] = BOARD_PIN_BUTTON_2,
        [BUTTON_3] = BOARD_PIN_BUTTON_3,
        [BUTTON_4] = BOARD_PIN_BUTTON_4
    };
    static struct {
        bool state;
        uint32_t downTick;
    } button[BUTTON_COUNT] = {{0}};
    static uint32_t lastButtonTick = 0;
    if (boardGetCurrentTick() - lastButtonTick >= 1) {
        lastButtonTick = boardGetCurrentTick();
        for (uint32_t i = 0; i < BUTTON_COUNT; i++) {
            bool state = boardGetPin(ButtonPin[i]);
            if (state != button[i].state) {
                if (button[i].state == true && state == false 
                    && button[i].downTick < BUTTON_HOLD_TIME_MS) {
                    processButtonEvent(i, BUTTON_EVENT_PRESS);
                }
                button[i].state = state;
                button[i].downTick = 0;
            }

            if (button[i].state) {
                uint32_t prevTick = button[i].downTick;
                button[i].downTick++;
                if (prevTick < BUTTON_HOLD_TIME_MS 
                    && button[i].downTick >= BUTTON_HOLD_TIME_MS) {
                    processButtonEvent(i, BUTTON_EVENT_HOLD);
                }
            } else {
                button[i].downTick = false;
            }
        }
    }
}

static void updateDisplay(void)
{
    static uint32_t lastDisplayTick = 0;

    if (boardGetCurrentTick() - lastDisplayTick >= FRAME_UPDATE_PERIOD_MS) {
        lastDisplayTick = boardGetCurrentTick();
        gfxFill(GFX_COLOR_BLACK);
        drawTicker();
        switch (currentMode) {
        case MODE_NORMAL:
            drawNormalMode();
            break;
        case MODE_TIMER_DOWN:
            drawTimerDown();
            break;
        case MODE_TIMER_UP:
            drawTimerUp();
            break;
        case MODE_SET_TIME:
            drawSetTime();
            break;
        default:
            break;
        }
        gfxFlush();
    }
}

void clockDisplayInit(void)
{
    isClockNotSet = boardWasRtcReset();
    TimeUtilTime countdownHMS = {.hour = 0, .minute = 5, .second = 0};
    countDownTime = timeUtilTimeToCounter(&countdownHMS) + 1;
    enterMode(MODE_NORMAL);
}

void clockDisplayProcess(void)
{
    updateButtons();
    updateMode();
    updateTicker();
    updateDisplay();
}