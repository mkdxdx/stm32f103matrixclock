#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef enum {
    LED_MATRIX_SEGMENT_COLOR_GREEN,
    LED_MATRIX_SEGMENT_COLOR_RED,
    LED_MATRIX_SEGMENT_COLOR_COUNT
} LedMatrixSegmentColor;

#define MATRIX_COLUMNS (32U)
#define MATRIX_ROWS (7U)

void ledMatrixInit(void);
void ledMatrixSetPixel(LedMatrixSegmentColor color, uint32_t x, uint32_t y, 
                         bool set);
void ledMatrixSetColumn(LedMatrixSegmentColor color, uint32_t column, uint8_t data);