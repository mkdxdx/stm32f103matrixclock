#include "gfx.h"
#include "ledMatrix.h"
#include "lucidaFont.h"
#include <stdint.h>
#include <stddef.h>
#include <string.h>

extern FONT_INFO liberationMono_8ptFontInfo;
extern FONT_CHAR_INFO liberationMono_8ptDescriptors;
extern uint8_t liberationMono_8ptBitmaps[]; 

static uint8_t matrixData[LED_MATRIX_SEGMENT_COLOR_COUNT][MATRIX_COLUMNS] = {0};

inline static void matrixSet(LedMatrixSegmentColor color, 
                             uint32_t x, uint32_t y)
{
    matrixData[color][x] |= 1 << y;
}

inline static void matrixClear(LedMatrixSegmentColor color, 
                               uint32_t x, uint32_t y)
{
    matrixData[color][x] &= ~(1 << y);
}

inline static void refreshMatrixColumn(uint32_t column)
{
    for (LedMatrixSegmentColor color = (LedMatrixSegmentColor)0; color < LED_MATRIX_SEGMENT_COLOR_COUNT; color++) {
        ledMatrixSetColumn(color, column, matrixData[color][column]);
    }
}

static void getCharSize(char c, GfxPoint *p)
{
    FONT_INFO *font = &liberationMono_8ptFontInfo;
    FONT_CHAR_INFO *desc = &liberationMono_8ptDescriptors;

    if (p == NULL) {
        return;
    }

    if (c == ' ') {
        p->x = font->spacePixels;
        p->y = font->height;
    } else if (c >= font->startChar && c <= font->endChar) {
        FONT_CHAR_INFO *charData = &desc[c - font->startChar];
        p->x = charData->widthBits;
        p->y = font->height;
    }
}

static void putChar(char c, const GfxPoint *p, GfxColor color)
{
    if (p == NULL) {
        return;
    }

    FONT_INFO *font = &liberationMono_8ptFontInfo;
    FONT_CHAR_INFO *desc = &liberationMono_8ptDescriptors;

    if (c == ' ') {
        for (uint32_t i = 0; i < font->spacePixels; i++) {
            for (uint32_t y = 0; y < font->height; y++) {
                GfxPoint pos = {p->x + i, y};
                gfxSetPixel(&pos, color);
            }
        }
    } else if (c >= font->startChar && c <= font->endChar) {
        FONT_CHAR_INFO *charData = &desc[c - font->startChar];
        uint8_t *bitmap = &liberationMono_8ptBitmaps[charData->offset];
        for (uint32_t i = 0; i < charData->widthBits; i++) {
            for (uint32_t y = 0; y < font->height; y++) {
                if ((bitmap[i] & (1 << y)) != 0) {
                    GfxPoint pos = {p->x + i, p->y + y};
                    gfxSetPixel(&pos, color);
                }
            }
        }
    }
}

void gfxInit(void)
{
    ledMatrixInit();
}

void gfxSetPixel(const GfxPoint *p, GfxColor color)
{
    if (p == NULL || p->x >= MATRIX_COLUMNS || p->y >= MATRIX_ROWS) {
        return;
    }

    switch (color) {
    case GFX_COLOR_BLACK:
        matrixClear(LED_MATRIX_SEGMENT_COLOR_GREEN, p->x, p->y);
        matrixClear(LED_MATRIX_SEGMENT_COLOR_RED, p->x, p->y);
        break;
    case GFX_COLOR_RED:
        matrixSet(LED_MATRIX_SEGMENT_COLOR_RED, p->x, p->y);
        matrixClear(LED_MATRIX_SEGMENT_COLOR_GREEN, p->x, p->y);
        break;
    case GFX_COLOR_GREEN:
        matrixSet(LED_MATRIX_SEGMENT_COLOR_GREEN, p->x, p->y);
        matrixClear(LED_MATRIX_SEGMENT_COLOR_RED, p->x, p->y);
        break;
    case GFX_COLOR_YELLOW:
        matrixSet(LED_MATRIX_SEGMENT_COLOR_GREEN, p->x, p->y);
        matrixSet(LED_MATRIX_SEGMENT_COLOR_RED, p->x, p->y);
        break;
    default:
        return;
    }
}

void gfxPuts(char *str, const GfxPoint *p, GfxColor color)
{
    if (p == NULL) {
        return;
    }

    GfxPoint pos = *p;
    while (*str) {
        GfxPoint charSize = {0, 0};
        getCharSize(*str, &charSize);
        putChar(*(str++), &pos, color);
        // add y increment?
        pos.x += charSize.x;
    }
}

void gfxGetStringSize(char *str, GfxPoint *size)
{
    if (str == NULL || size == NULL) {
        return;
    }

    size->x = 0;
    size->y = 0;
    while (*str) {
        GfxPoint charSize = {0, 0};
        getCharSize(*(str++), &charSize);
        size->x += charSize.x;
        // add height increment?
        size->y = charSize.y;
    }
}

void gfxFill(GfxColor color)
{
    uint8_t fillValue[LED_MATRIX_SEGMENT_COLOR_COUNT];
    switch (color) {
    case GFX_COLOR_BLACK:
        fillValue[LED_MATRIX_SEGMENT_COLOR_GREEN] = 0;
        fillValue[LED_MATRIX_SEGMENT_COLOR_RED] = 0;
        break;
    case GFX_COLOR_RED:
        fillValue[LED_MATRIX_SEGMENT_COLOR_GREEN] = 0;
        fillValue[LED_MATRIX_SEGMENT_COLOR_RED] = 0x7F;
        break;
    case GFX_COLOR_GREEN:
        fillValue[LED_MATRIX_SEGMENT_COLOR_GREEN] = 0x7F;
        fillValue[LED_MATRIX_SEGMENT_COLOR_RED] = 0;
        break;
    case GFX_COLOR_YELLOW:
        fillValue[LED_MATRIX_SEGMENT_COLOR_GREEN] = 0x7F;
        fillValue[LED_MATRIX_SEGMENT_COLOR_RED] = 0x7F;
        break;
    default:
        return;
    }

    for (LedMatrixSegmentColor i = (LedMatrixSegmentColor)0; i < LED_MATRIX_SEGMENT_COLOR_COUNT; i++) {
        memset(matrixData[i], fillValue[i], sizeof(matrixData[i]));
    }
}

void gfxGetScreenSize(GfxPoint *size)
{
    if (size == NULL) {
        return;
    }

    size->x = MATRIX_COLUMNS;
    size->y = MATRIX_ROWS;
}

void gfxFlush(void)
{
    for (uint32_t column = 0; column < MATRIX_COLUMNS; column++) {
        refreshMatrixColumn(column);
    }
}