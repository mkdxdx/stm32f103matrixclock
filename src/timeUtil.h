#pragma once

#include <stdint.h>

typedef struct {
    uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t monthDay;
	uint8_t weekDay;
	uint8_t month;
	uint16_t year;
} TimeUtilDate;

typedef struct {
    uint8_t second;
    uint8_t minute;
    uint32_t hour;
} TimeUtilTime;

void timeUtilCounterToDate(uint32_t counter, TimeUtilDate *date);
uint32_t timeUtilDateToCounter(TimeUtilDate *date);
void timeUtilCounterToTime(uint32_t counter, TimeUtilTime *time);
uint32_t timeUtilTimeToCounter(TimeUtilTime *time);